const   express   = require("express"),
        app         = express()

app.get("/", (req, res)=>{
    res.send({
        response: "200"
    })
})

app.listen(process.env.PORT || 3000)